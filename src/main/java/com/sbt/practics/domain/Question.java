package com.sbt.practics.domain;

import javax.persistence.*;

@Entity
@Table (name = "questions")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "question")
    private String description;
    @OneToOne
    @JoinColumn(name = "next_negative_question_id", referencedColumnName = "id")
    private Question nextNegativeQuestion;
    @OneToOne
    @JoinColumn(name = "next_positive_question_id", referencedColumnName = "id")
    private Question nextPositiveQuestion;
    @OneToOne
    @JoinColumn(name = "solution_id", referencedColumnName = "id", nullable = true)
    private Solution solution;

    public Question() {}

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public Question getNextNegativeQuestion() {
        return nextNegativeQuestion;
    }

    public Question getNextPositiveQuestion() {
        return nextPositiveQuestion;
    }

    public Solution getSolution() {
        return solution;
    }
}