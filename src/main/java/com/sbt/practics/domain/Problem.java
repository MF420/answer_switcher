package com.sbt.practics.domain;

import javax.persistence.*;

@Entity
@Table(name = "problems")
public class Problem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "problem")
    private String description;
    @OneToOne
    @JoinColumn(name = "question_id", referencedColumnName = "id")
    private Question firstQuestion;

    public Problem() {}

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public Question getFirstQuestion() {
        return firstQuestion;
    }
}