package com.sbt.practics.domain;

import javax.persistence.*;

@Entity
@Table (name = "solutions")
public class Solution {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "solution")
    private String description;

    public Solution() {}

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }
}