package com.sbt.practics.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sbt.practics.domain.Question;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepo extends JpaRepository<Question, Integer> {
}