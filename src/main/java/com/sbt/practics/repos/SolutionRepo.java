package com.sbt.practics.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sbt.practics.domain.Solution;
import org.springframework.stereotype.Repository;

@Repository
public interface SolutionRepo extends JpaRepository<Solution, Integer> {
}