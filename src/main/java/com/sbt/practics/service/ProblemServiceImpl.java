package com.sbt.practics.service;

import com.sbt.practics.domain.Problem;
import com.sbt.practics.repos.ProblemRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProblemServiceImpl implements ProblemService {

    @Autowired
    private ProblemRepo problemRepo;

    public List<Problem> getAllProblems() {
        return problemRepo.findAll().stream().collect(Collectors.toList());
    }
}