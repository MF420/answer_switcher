package com.sbt.practics.service;

import com.sbt.practics.domain.Question;
import com.sbt.practics.domain.Solution;

import org.springframework.stereotype.Service;

@Service
public interface QuestionService {

    Question getQuestionById(int id);

    boolean isDeadEnd(Question question);

    boolean isDeadEnd(int questionId);

    Solution getSolution(int id);

}