package com.sbt.practics.service;

import com.sbt.practics.domain.Question;
import com.sbt.practics.domain.Solution;
import com.sbt.practics.repos.QuestionRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionRepo questionRepo;

    public Question getQuestionById(int id) {
        Optional<Question> question = questionRepo.findById(id);
        return question.isPresent() ? question.get() : null;
    }

    public boolean isDeadEnd(Question question) {
        return question.getNextPositiveQuestion() == null & question.getNextNegativeQuestion() == null;
    }

    public boolean isDeadEnd(int questionId) {
        return isDeadEnd(getQuestionById(questionId));
    }

    public Solution getSolution(int id) {
        return getQuestionById(id).getSolution();
    }
}