package com.sbt.practics.service;

import com.sbt.practics.domain.Problem;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProblemService {

    List<Problem> getAllProblems();

}