package com.sbt.practics.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class SolutionPageController {
    @GetMapping("/solution")
    public String displaySolution(@ModelAttribute("solutionObject") Object solution, Model model) {
        model.addAttribute("solution", solution);

        return "solution";
    }
}