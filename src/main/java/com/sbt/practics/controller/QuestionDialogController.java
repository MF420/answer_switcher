package com.sbt.practics.controller;

import com.sbt.practics.service.QuestionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class QuestionDialogController {
    @Autowired
    private QuestionService questionService;

    @GetMapping("/question")
    public String getQuestionDialog(@RequestParam int id, RedirectAttributes attributes, Model model) {
        if (questionService.isDeadEnd(id)) {
            attributes.addFlashAttribute("solutionObject", questionService.getSolution(id));
            return "redirect:solution";
        }

        model.addAttribute("question", questionService.getQuestionById(id));

        return "question";
    }
}