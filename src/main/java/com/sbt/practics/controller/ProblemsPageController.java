package com.sbt.practics.controller;

import com.sbt.practics.service.ProblemService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProblemsPageController {
    @Autowired
    private ProblemService problemService;

    @GetMapping("/")
    public String displayMainPageButtons(Model model) {
        model.addAttribute("problems", problemService.getAllProblems());

        return "main";
    }
}